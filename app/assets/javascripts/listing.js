// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
var jPM;
$(function () {
  $('[data-toggle="popover"]').popover();
  	jPM = $.jPanelMenu({excludedPanelContent: ".navbar"});
  	jPM.off();
		jPM.on();
	$("#light-slider").lightSlider({
		item: 1,
		thumbItem: 15,
		gallery: true,
		nextHtml: '<div><span class="glyphicon glyphicon-chevron-right"></span></div>',
		prevHtml: '<div><span class="glyphicon glyphicon-chevron-left"></span></div>'

	});
	$("#trigger").click(function(){
		if (jPM.isOpen()) {
			jPM.close();
		} else {
			jPM.open();
		}
	});
})

