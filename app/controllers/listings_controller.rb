class ListingsController < ApplicationController

  before_action :authenticate_user!, only: [:new, :edit]

  def index
    @listings = Listing.all
  end

  def show
    @listing = Listing.find(params[:id]);
  end

  def new
    @listing = Listing.new
  end

  def create
    # render plain: params
    @listing = Listing.new(listing_params)
    @listing.user = current_user
    if @listing.save
      flash[:success] = "Listing created successfully"
      redirect_to listing_path(@listing)
    else
      render 'new'
    end
  end

  def edit
    @listing = Listing.find(params[:id]);
  end

  def update
    @listing = Listing.find(params[:id]);
    if @listing.update(listing_params)
      flash[:success] = "Listing updated successfully"
      redirect_to listing_path(@listing)
    else
      render 'edit'
    end
  end

  def destroy
  end

  private
  def listing_params
    params.require(:listing).permit(
      :borough, :address, :unit, :property_type, :size, :bedrooms, :bathrooms, :rooms,
      :website, :description, :price, :taxes, :maintenance, :balcony,
      :courtyard, :dishwasher, :fireplace, :furnished, :loft, :ac,
      :recessed_lighting, :washer_dryer_in_unit, :lounge, :doorman,
      :swimming_pool, :elevator, :gym, :laundry_in_building, :parking_on_site,
      :pets_allowed, :roof_deck, :street_parking_available,
      :free_wifi_in_lobby, :security_cameras, photos_attributes:[:image, :_destroy,:id])
  end
end
