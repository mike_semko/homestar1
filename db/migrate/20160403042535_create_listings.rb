class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
    	t.string :borough
    	t.string :address
    	t.string :unit
    	t.string :type
    	t.integer :size
    	t.integer :bedrooms
    	t.integer :bathrooms
    	t.integer :rooms
    	t.string :website
    	t.text :description
    	t.float :price
    	t.float :taxes
    	t.float :maintenance
    	t.boolean :balcony
    	t.boolean :courtyard
    	t.boolean :dishwasher
    	t.boolean :fireplace
    	t.boolean :furnished
    	t.boolean :loft
    	t.boolean :ac
    	t.boolean :recessed_lighting
    	t.boolean :washer_dryer_in_unit
    	t.boolean :lounge
    	t.boolean :doorman
    	t.boolean :swimming_pool
    	t.boolean :elevator
    	t.boolean :gym
    	t.boolean :laundry_in_building
    	t.boolean :parking_on_site
    	t.boolean :pets_allowed
    	t.boolean :roof_deck
    	t.boolean :street_parking_available
    	t.timestamps
    end
  end
end
