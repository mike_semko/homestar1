class AddFieldsToListings < ActiveRecord::Migration
  def change
    add_column :listings, :free_wifi_in_lobby, :boolean
    add_column :listings, :security_cameras, :boolean
  end
end
