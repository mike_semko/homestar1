# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160428185319) do

  create_table "listings", force: :cascade do |t|
    t.string   "borough"
    t.string   "address"
    t.string   "unit"
    t.string   "property_type"
    t.integer  "size"
    t.integer  "bedrooms"
    t.integer  "bathrooms"
    t.integer  "rooms"
    t.string   "website"
    t.text     "description"
    t.float    "price"
    t.float    "taxes"
    t.float    "maintenance"
    t.boolean  "balcony"
    t.boolean  "courtyard"
    t.boolean  "dishwasher"
    t.boolean  "fireplace"
    t.boolean  "furnished"
    t.boolean  "loft"
    t.boolean  "ac"
    t.boolean  "recessed_lighting"
    t.boolean  "washer_dryer_in_unit"
    t.boolean  "lounge"
    t.boolean  "doorman"
    t.boolean  "swimming_pool"
    t.boolean  "elevator"
    t.boolean  "gym"
    t.boolean  "laundry_in_building"
    t.boolean  "parking_on_site"
    t.boolean  "pets_allowed"
    t.boolean  "roof_deck"
    t.boolean  "street_parking_available"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "free_wifi_in_lobby"
    t.boolean  "security_cameras"
    t.integer  "user_id"
  end

  create_table "photos", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "listing_id"
  end

  add_index "photos", ["listing_id"], name: "index_photos_on_listing_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "display_name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
