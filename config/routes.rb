Rails.application.routes.default_url_options[:host] = 'localhost:3000'

Rails.application.routes.draw do
  devise_for :users, controllers: {
  	registrations: "users/registrations"
  }
	root "listings#index"
  resources :listings
end

